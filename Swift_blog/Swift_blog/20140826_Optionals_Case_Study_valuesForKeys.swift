//
//  20140826_Optionals_Case_Study_valuesForKeys.swift
//  Swift_blog
//
//  Created by yao_yu on 14-8-31.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation

/*
2014-08-26
Optionals研究:valuesForKeys
本文展示在swift中optionals如何保证强类型安全.我们将创建一个Swift版本的Objective-C API.Swift实际上并不需要这个API, 但它是有趣的例子.

在Objective-C, NSDictionary有一个方法 -objectsForKey:notFoundMarker:, 它传入关键字NSArray, 返回相应值NSArray. 文档说到:"返回数组中的第N个对象对应输入数组中第N个关键字的值". 如果关键字不在字典中呢? 这时notFoundMarker参数就起作用了.
*/

extension Dictionary{
    func valuesForKeys2(keys:[Key]) -> [Value?]{
        return keys.map{self[$0]}
    }
}

func test_dictionary(){
    let dict = ["A": "Amir", "B": "Bertha", "C": "Ching"]

    println(dict.valuesForKeys2(["A", "C"]))
    // [Optional("Amir"), Optional("Ching")]

    println(dict.valuesForKeys2(["B", "D"]))
    // [Optional("Bertha"), nil]

    println(dict.valuesForKeys2([]))
    // []

    println(dict.valuesForKeys2(["A", "C"]).last)
    // [Optional("Amir"), Optional("Ching")]

    println(dict.valuesForKeys2(["B", "D"]).last)
    // [Optional("Bertha"), nil]

    println(dict.valuesForKeys2([]).last)
    // []
}
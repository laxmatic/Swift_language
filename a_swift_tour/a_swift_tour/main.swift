//
//  main.swift
//  a_swift_tour(The Swift Programming Language)(2014-8-4)
//
//  Created by yao_yu on 14-8-16.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation


// P5
//println("Hello, World!")

func main(){
    ControlFlow()
    FunctionsAndClosures()
    ClassAndObject()
    EnumerationsAndStructures()
    ProtocolsAndExtensions()
    GenericsTest()
}

main()
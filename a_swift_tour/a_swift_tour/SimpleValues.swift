//
//  SimpleValues.swift
//  a_swift_tour
//
//  Created by yao_yu on 14-8-18.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//


import Foundation

// P6 简单值
func SimpleValue(){
    // P6 简单值
    var myVariable = 42     // 变量
    myVariable = 50
    let myConstant = 42     // 常量
    
    // P7 变量(常量)声明
    let implicitInteger = 70            // 隐式声明整型数
    let implicitDouble = 70.0           // 隐式声明双精度浮点数
    let explicitDouble: Double = 70     // 显示声明双精度浮点数
    let explicitFloat: Float = 4        // 显示声明单精度浮点数
    
    // P8 数据转换
    let label = "The width is "
    let width = 94
    let widthLabel = label + String(width)  // 使用字符串构造函数进行转换
    
    // P8 使用 \() 使数据转换到字符串
    let apples = 3
    let oranges = 5
    let appleSummary = "I have \(apples) apples."
    let orangesSummary = "I have \(oranges) oranges."
    let fruitSummary = "I have \(apples + oranges) pieces of fruit."
    
    // 浮点数位数目前无法控制(缺陷)
    let doubleValue = 3.732232334334343434344344
    let doubleSummary = "double \(doubleValue)."
    
    // P9 数组和字典
    var shoppingList = ["catfish", "water", "tulips", "blue paint"]
    shoppingList[1] = "bottle of water"
    
    var occupations = [
        "Malcolm": "Captain",
        "Kaylee": "Mechanic",
    ]
    occupations["Jayne"] = "Public Relations"
    
    // P10 未知类型的数组和字典初始化
    let emptyArray = [String]()
    let emptyDictionary = [String:Float]()
    
    // P10 已知类型的数组和字典初始化
    shoppingList = []
    occupations = [:]
}

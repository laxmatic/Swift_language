//
//  EnumeratinosAndStructures.swift
//  a_swift_tour
//
//  Created by yao_yu on 14-8-18.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation


// P35 枚举和结构
func EnumerationsAndStructures(){
    let ace = Rank.Ace
    let aceRowValue = ace.toRaw()
    //println()
    let b = Rank.Ace
    assert(ace == b, "值不相等")
    
    if let convertedRank = Rank.fromRaw(1){
        var desc = convertedRank.simpleDescription()
        //println(desc)
    }
    
    //P38
    let hearts = Suit.Hearts
    let heartsDescription = hearts.simpleDescription()
    //println(hearts.color)
    //println(heartsDescription)
    
    // P40
    let threeOfSpades = Card(rank: .Three, suit: .Spades)
    //println(threeOfSpades.simpleDescription())
    let cards = Card.cards()
    for card in cards{
        //println(card.simpleDescription())
    }
    
    // P42
    var success = ServerResponse.Result("6:00 am", "8:09 pm")
    success = ServerResponse.Status(200, "查询成功")
    func test(success:ServerResponse){
        var failure = ServerResponse.Error("Out of cheese.")
        var response = ""
        switch success{
        case let .Result(sunrise, sunset):
            response = "Sunrise is at \(sunrise) and sunset is at \(sunset)"
        case let .Error(error):
            response = "Failure... \(error)"
        case let ServerResponse.Status(code, status):
            response = "\(code), \(status)"
        }
        //println(response)
    }
    test(success)
}

// P36
enum Rank:Int{
    case Ace = 1
    case Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten
    case Jack, Queen, King
    func simpleDescription() -> String{
        switch self{
        case .Ace:
            return "ace"
        case .Jack:
            return "jack"
        case .Queen:
            return "queen"
        case .King:
            return "king"
        default:
            return String(toRaw())
        }
    }
}

func ==(lhs:Rank, rhs:Rank) -> Bool{
    return lhs.toRaw() == rhs.toRaw()
}

// P38
enum Suit{
    case Spades, Hearts, Diamonds, Clubs
    func simpleDescription() -> String{
        switch self{
        case .Spades:
            return "spades"
        case .Hearts:
            return "hearts"
        case .Diamonds:
            return "diamonds"
        case .Clubs:
            return "clubs"
        }
    }
    // P39 练习
    var color:String{
        switch self{
        case .Spades, .Clubs:
            return "back"
        default:
            return "red"
        }
    }
}

// P40 结构
struct Card{
    var rank:Rank
    var suit:Suit
    func simpleDescription() -> String{
        return "The \(rank.simpleDescription()) of \(suit.simpleDescription())"
    }
    // P40生成数组
    static func cards()->[Card]{
        var cards = [Card]()
        for _suit in [Suit.Spades, Suit.Hearts, Suit.Diamonds, Suit.Clubs]{
            for i in Rank.Ace.toRaw()...Rank.King.toRaw(){
                cards.append(Card(rank: Rank.fromRaw(i)!, suit: _suit))
            }
        }
        return cards
    }
}

// P42 高级枚举
enum ServerResponse{
    case Result(String, String)
    case Status(Int,String)
    case Error(String)
}

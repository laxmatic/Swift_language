//
//  ControlFlow.swift
//  a_swift_tour
//
//  Created by yao_yu on 14-8-18.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import Foundation


// P10 控制流 (if switch, for-in, for, while, do-while)
func ControlFlow(){
    // P11 for-in, if
    let individualScores = [75, 43, 103, 87, 12]
    var teamScore = 0
    for score in individualScores{
        if score > 50{
            teamScore += 3
        } else {
            teamScore += 1
        }
    }
    
    // P12 optional值
    var optionalString:String? = "Hello"
    optionalString = nil
    
    var optionalName: String? = "John Appleseed"
    var greeting = "Hello!"
    optionalName = nil
    if let name = optionalName{
        greeting = "Hello, \(name)"
    }
    
    // P14 switch
    var vegetable = "red pepper"
    var vegetableComment = ""
    switch vegetable{
    case "celery":
        vegetableComment = "Add some raisins and make ants on a log."
    case "cucumber", "watercress":
        vegetableComment = "That would make a good tea sandwith."
    case let x where x.hasSuffix("pepper"):
        vegetableComment = "Is it a splicy \(x)?"
    default:
        vegetableComment = "Everything tastes good in soup."
    }
    
    // P16 for-in
    let interestingNumbers = [
        "Prime": [2,3,5,7,11,13],
        "Fibonacci": [1,1,2,3,5,8],
        "Square": [1,4,9,16,25],
    ]
    var largest = 0
    for (kind,numbers) in interestingNumbers{
        for number in numbers{
            if number > largest{
                largest = number
            }
        }
    }
    //println(largest)
    
    // P17 while (do-while)
    var n = 2
    while n < 100{
        n *= 2
    }
    
    var m = 2
    do{
        m *= 2
    } while m < 100
    
    // P18 ..< 范围
    var firstForLoop = 0
    for i in 0..<4{
        firstForLoop += 1
    }
    //println(firstForLoop)
    
    // 原始 for
    var secondForLoop = 0
    for var i = 0; i < 4; ++i{
        secondForLoop += 1
    }
    //println(secondForLoop)
}
